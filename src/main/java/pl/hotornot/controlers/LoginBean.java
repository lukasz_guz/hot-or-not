package pl.hotornot.controlers;

import java.util.Map;
import java.util.Optional;

import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.hotornot.entity.ApplicationUser;
import pl.hotornot.services.UserService;

@RequestScoped
@Component
public class LoginBean {

	@Autowired
	private UserService userService;

	private String userName;
	private String password;
	private ApplicationUser newUser = new ApplicationUser();

	public String login() {
		Optional<ApplicationUser> user = userService.login(userName, password);
		user.ifPresent(u -> fillSessionParameters(u));
		return "/pages/home";
	}

	public String logOut() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/pages/login";
	}

	private void fillSessionParameters(ApplicationUser user) {
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sessionMap.put("username", user.getLogin());
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ApplicationUser getNewUser() {
		return newUser;
	}

	public void setNewUser(ApplicationUser newUser) {
		this.newUser = newUser;
	}
}