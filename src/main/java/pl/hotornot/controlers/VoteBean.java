package pl.hotornot.controlers;

import java.util.Optional;

import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.hotornot.entity.CompetitivePair;
import pl.hotornot.services.VoteService;

@ViewScoped
@Component
public class VoteBean {

	@Autowired
	private VoteService voteService;
	private CompetitivePair competitivePair;
	private boolean existCompetitivePair = false;

	public void init() {
		existCompetitivePair = false;
		Optional<CompetitivePair> nextPair = voteService.getNextPair();
		nextPair.ifPresent(pair -> {
			competitivePair = pair;
			existCompetitivePair = true;
		});
	}

	public void leftVote(ActionEvent actionEvent) {
		voteService.leftVote(competitivePair.getId());
	}

	public void rightVote(ActionEvent actionEvent) {
		voteService.rightVote(competitivePair.getId());
	}

	public CompetitivePair getCompetitivePair() {
		return competitivePair;
	}

	public void setCompetitivePair(CompetitivePair competitivePair) {
		this.competitivePair = competitivePair;
	}

	public boolean isExistCompetitivePair() {
		return existCompetitivePair;
	}

	public void setExistCompetitivePair(boolean existCompetitivePair) {
		this.existCompetitivePair = existCompetitivePair;
	}
}