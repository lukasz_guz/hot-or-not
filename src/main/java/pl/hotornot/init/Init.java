package pl.hotornot.init;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.hotornot.entity.ApplicationUser;
import pl.hotornot.entity.CompetitivePair;
import pl.hotornot.repository.CompetitivePairRepository;
import pl.hotornot.repository.UserRepository;

@Component
public class Init {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CompetitivePairRepository cpRepository;

	@PostConstruct
	private void init() {
		createUser();
		createCompetitivePair();
	}

	@Transactional
	private void createUser() {
		String login = "admin";
		if (userRepository.findByLogin(login) != null) {
			return;
		}

		ApplicationUser user = new ApplicationUser();
		user.setFirstName("Admin");
		user.setLastName("Admin");
		user.setLogin(login);
		user.setPassword(login);

		userRepository.save(user);
	}

	@Transactional
	private void createCompetitivePair() {
		if (cpRepository.findOne(1L) != null || cpRepository.findOne(2L) != null) {
			return;
		}

		CompetitivePair cp = new CompetitivePair();
		cp.setId(1L);
		cp.setLeftPicture("girl1.jpeg");
		cp.setRightPicture("girl2.jpeg");
		cpRepository.saveAndFlush(cp);

		cp = new CompetitivePair();
		cp.setId(2L);
		cp.setLeftPicture("girl2.jpeg");
		cp.setRightPicture("girl1.jpeg");

		cpRepository.saveAndFlush(cp);
	}
}