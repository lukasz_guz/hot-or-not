package pl.hotornot.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@SuppressWarnings("serial")
@Entity
public class ApplicationUser implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String login;
	private String password;
	private String firstName;
	private String lastName;

	@ManyToMany(mappedBy = "applicationUsers")
	private Set<CompetitivePair> votes = new HashSet<>();

	public Set<CompetitivePair> getVotes() {
		return votes;
	}

	public void setVotes(Set<CompetitivePair> votes) {
		this.votes = votes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}