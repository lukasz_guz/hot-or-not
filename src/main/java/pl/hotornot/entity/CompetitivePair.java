package pl.hotornot.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@SuppressWarnings("serial")
@Entity
public class CompetitivePair implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	private String leftPicture;

	private int leftVote = 0;

	private String rightPicture;

	private int rightVote = 0;

	@ManyToMany(cascade = CascadeType.ALL, targetEntity = ApplicationUser.class)
	private Set<ApplicationUser> applicationUsers = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLeftVote() {
		return leftVote;
	}

	public void setLeftVote(int leftVote) {
		this.leftVote = leftVote;
	}

	public int getRightVote() {
		return rightVote;
	}

	public void setRightVote(int rightVote) {
		this.rightVote = rightVote;
	}

	public String getLeftPicture() {
		return leftPicture;
	}

	public void setLeftPicture(String leftPicture) {
		this.leftPicture = leftPicture;
	}

	public String getRightPicture() {
		return rightPicture;
	}

	public Set<ApplicationUser> getApplicationUsers() {
		return applicationUsers;
	}

	public void setApplicationUsers(Set<ApplicationUser> applicationUsers) {
		this.applicationUsers = applicationUsers;
	}

	public void setRightPicture(String rightPicture) {
		this.rightPicture = rightPicture;
	}
}