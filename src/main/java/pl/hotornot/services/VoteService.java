package pl.hotornot.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.hotornot.entity.ApplicationUser;
import pl.hotornot.entity.CompetitivePair;
import pl.hotornot.repository.CompetitivePairRepository;

@Service
public class VoteService {

	@Autowired
	private UserService userService;

	@Autowired
	private CompetitivePairRepository competitivePairRepository;

	public Optional<CompetitivePair> getNextPair() {
		Optional<ApplicationUser> loggedUser = userService.getLoggedUser();
		Optional<CompetitivePair> competitivePair = Optional.empty();
		if (loggedUser.isPresent()) {
			competitivePair = Optional.ofNullable(competitivePairRepository.findOneNotVotePerUser(loggedUser.get().getId()));
		}
		return competitivePair;
	}

	@Transactional
	public void leftVote(Long competitivePairId) {
		CompetitivePair cp = competitivePairRepository.findOne(competitivePairId);
		cp.setLeftVote(cp.getLeftVote() + 1);
		saveCompetitivePair(cp);
	}

	@Transactional
	public void rightVote(Long competitivePairId) {
		CompetitivePair cp = competitivePairRepository.findOne(competitivePairId);
		cp.setRightVote(cp.getRightVote() + 1);
		saveCompetitivePair(cp);
	}

	@Transactional
	public Optional<CompetitivePair> saveCompetitivePair(CompetitivePair cp) {
		ApplicationUser user = userService.getLoggedUser().get();
		cp.getApplicationUsers().add(user);
		user.getVotes().add(cp);
		cp = competitivePairRepository.saveAndFlush(cp);
		return Optional.of(cp);
	}
}