package pl.hotornot.services;

import java.util.Map;
import java.util.Optional;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.hotornot.entity.ApplicationUser;
import pl.hotornot.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public Optional<ApplicationUser> login(String login, String password) {
		ApplicationUser user = userRepository.findByLoginAndPassword(login, password);
		return Optional.ofNullable(user);
	}

	public Optional<ApplicationUser> getLoggedUser() {
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		String userLogin = (String) sessionMap.get("username");
		return Optional.ofNullable(userRepository.findByLogin(userLogin));
	}
}