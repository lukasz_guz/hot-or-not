package pl.hotornot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.hotornot.entity.CompetitivePair;

@Repository
public interface CompetitivePairRepository extends JpaRepository<CompetitivePair, Long> {

	// @Query(nativeQuery = true, value =
	// "SELECT c.* from competitivepair c left join competitivepair_applicationuser ap on c.id = ap.applicationusers_id WHERE ap IS NULL OR ap.applicationusers_id <> :userId LIMIT 1")
	@Query(nativeQuery = true, value = "SELECT c.* FROM competitivepair c left join competitivepair_applicationuser ap on c.id = ap.votes_id WHERE ap.applicationusers_id <> :userId OR ap IS NULL LIMIT 1")
	CompetitivePair findOneNotVotePerUser(@Param("userId") Long userId);
}