package pl.hotornot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.hotornot.entity.ApplicationUser;

@Repository
public interface UserRepository extends JpaRepository<ApplicationUser, Long> {

	@Query("SELECT u FROM ApplicationUser u WHERE u.login=:login AND u.password=:password")
	ApplicationUser findByLoginAndPassword(@Param("login") String login, @Param("password") String password);

	ApplicationUser findByLogin(String login);

}